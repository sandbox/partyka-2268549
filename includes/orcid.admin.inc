<?php

/**
 * @file
 * UI Controller for Orcid Entity. Creates/manages Orcid fields, display modes.
 */

class OrcidUIController extends EntityDefaultUIController {

  /**
   * Override the menu hook for default ui controller.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['title'] = 'Orcid Authors';
    $items[$this->path]['description'] = 'Manage Orcid Authors.';
    $items[$this->path]['access callback'] = TRUE;
    $items[$this->path]['type'] = MENU_LOCAL_TASK;
    return $items;
  }

  /**
   * Admin form for searching and doing bulk operations.
   */
  public function overviewForm($form, &$form_state) {
    $form['pager'] = array('#theme' => 'pager');

    $header = array(
      'orcid_id' => array(
        'data' => t('ORCID ID'),
        'field' => 'orcid_id',
      ),
      'first_name' => array(
        'data' => t('Given Name'),
        'field' => 'first_name',
      ),
      'last_name' => array(
        'data' => t('Family Name'),
        'field' => 'last_name',
      ),
    );

    $options = array();
    $search_term = !empty($_GET['search']) ? $_GET['search'] : NULL;

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'OrcidAuthor');

    if (!empty($search_term)) {
      $query->propertyCondition('orcid_id', '%' . $search_term . '%', 'like');
      // TODO: Add OR support so that the same field can also search in the
      // name fields.
    }

    // Check for sort order and sort key.
    if (!empty($_GET['sort']) && !empty($_GET['order'])) {
      $sort = strtoupper($_GET['sort']);
      $order = strtolower($_GET['order']);
      $order = str_replace(' ', '_', $order);
      if ($order != 'operations') {
        $query->propertyOrderBy($order, $sort);
      }
    }

    $query->pager(50);

    $result = $query->execute();
    $orcid_author_results = !empty($result['OrcidAuthor']) ? $result['OrcidAuthor'] : array();

    $orcid_keys = array();
    foreach ($orcid_author_results as $key => $value) {
      $orcid_keys[] = $key;
    }

    $results_array = !empty($orcid_author_results) ? entity_load('OrcidAuthor', $orcid_keys) : array();
    // $results_array = entity_load('OrcidAuthor', $orcid_keys);
    foreach ($results_array as $result_id => $results) {
      $options['orcid_id-' . $result_id] = array(
        'orcid_id' => l($results->orcid_id, 'orcid/author/' . $results->orcid_id),
        'first_name' => l($results->first_name, 'orcid/author/' . $results->orcid_id),
        'last_name' => l($results->last_name, 'orcid/author/' . $results->orcid_id),
      );
    }

    $form['search'] = array(
      '#type' => 'fieldset',
      '#title' => t('Basic Search'),
      '#collapsible' => TRUE,
      '#collapsed' => !empty($search_term) ? FALSE : TRUE,
    );

    $form['search']['search_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Search Term'),
      '#default_value' => !empty($search_term) ? $search_term : '',
    );

    $form['search']['search_submit'] = array(
      '#type' => 'submit',
      '#value' => t('Search'),
    );

    $form['entities'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#attributes' => array('class' => array('entity-sort-table')),
      '#empty' => t('There are no ORCID authors.'),
    );

    return $form;
  }

  /**
   * Form Submit method.
   */
  public function overviewFormSubmit($form, &$form_state) {
    $values = $form_state['values'];

    if (!empty($values['search_text'])) {
      drupal_goto('admin/content/orcid-authors', array('query' => array('search' => $values['search_text'])));
    }
  }

}
