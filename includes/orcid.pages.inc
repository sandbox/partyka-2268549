<?php

/**
 * @file
 * Various page callbacks as part of the orcid module.
 */

/**
 * Gets the author form.
 *
 * This form is used to allow the user to import data from ORCID.
 *
 * The only field that this form provides is a field to enter the ORCID
 * identity. This is because we are currently only reading from ORCID and
 * not writing to it.
 */
function orcid_add_author_form() {
  $form['orcid_id'] = array(
    '#type' => 'textfield',
    '#maxlength' => 19,
    '#required' => TRUE,
    // '#ajax' => 'orcid_validate_id',
    '#default_value' => "",
    '#size' => 32,
    '#title' => t("ORCID iD:"),
    '#description' => t('Please enter a valid ORCID iD. Press "Import" to import from ORCID.'),
  );

  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['import'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
    '#submit' => $submit + array('orcid_author_form_submit'),
  );

  return $form;
}

/**
 * Form API submit callback for the model form.
 *
 * @todo remove hard-coded link.
 */
function orcid_author_form_submit(&$form, &$form_state) {
  $redirect = TRUE;

  $orcid_author = entity_create("OrcidAuthor",
    array(
      "orcid_id" => check_plain($form_state['values']['orcid_id']),
      "first_name" => "",
      "last_name" => "",
      "published_name" => "",
      "email" => "",
    )
  );

  $wrapper = entity_metadata_wrapper("OrcidAuthor", $orcid_author);
  $wrapper->validate("orcid_id");

  // Save the model and go back to the add author screen.
  $transaction = db_transaction("orcid_author_import");
  try {
    $orcid_author->importAuthor();
    $wrapper->other_names->set($orcid_author->other_names);
    $wrapper->urls->set($orcid_author->urls);
    $orcid_author->save();
  }
  catch (Exception $e) {
    $transaction->rollback();
    form_set_error("orcid_id", $e->getMessage());
    $redirect = FALSE;
  }

  if ($redirect) {
    $form_state['redirect'] = 'orcid/author/'
      . check_plain($orcid_author->orcid_id);
  }
}

/**
 * Form constructor for the orcid author deletion confirmation form.
 *
 * @see orcid_author_delete_confirm_submit()
 */
function orcid_author_delete_confirm($form, &$form_state, $orcid_author) {
  $orcid_author = array_pop($orcid_author);
  $form['#orcid_author'] = $orcid_author;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['orcid_id'] = array(
    '#type' => 'value',
    '#value' => $orcid_author->orcid_id,
  );
  return confirm_form($form,
    t('Are you sure you want to delete %given_name %surname (%id)?',
      array(
        '%given_name' => $orcid_author->first_name,
        '%surname' => strtoupper($orcid_author->last_name),
        '%id' => $orcid_author->orcid_id,
      )
    ),
    'orcid/author' . $orcid_author->orcid_id,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Executes orcid author deletion.
 *
 * TODO: When publications are added to this module, this will need to check
 * if there are any publications referencing this author. If so, delete
 * may need to be declined. OR the author reference gets converted into
 * some other data-type. TBD.
 * @see orcid_author_delete_confirm()
 */
function orcid_author_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $orcid_author = orcid_author_load($form_state['values']['orcid_id']);
    orcid_delete_author($orcid_author);
    cache_clear_all();
    $orcid_author = array_pop($orcid_author);
    watchdog('orcid', '@type: deleted %id.', array(
      '@type' => "ORCID Author",
      '%id' => $orcid_author->orcid_id,
    ));
    drupal_set_message(t('@type %id has been deleted.', array(
          '@type' => "ORCID Author",
          '%id' => $orcid_author->orcid_id,
        )
      )
    );

  }

  $form_state['redirect'] = '<front>';
}

/**
 * Form constructor for the orcid author update confirmation form.
 *
 * @see orcid_author_delete_confirm_submit()
 */
function orcid_author_update_confirm($form, &$form_state, $orcid_author) {
  $orcid_author = array_pop($orcid_author);
  $form['#orcid_author'] = $orcid_author;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['orcid_id'] = array(
    '#type' => 'value',
    '#value' => $orcid_author->orcid_id,
  );
  return confirm_form($form,
    t('Are you sure you want to update %given_name %surname (%id)?',
      array(
        '%given_name' => $orcid_author->first_name,
        '%surname' => $orcid_author->last_name,
        '%id' => $orcid_author->orcid_id,
      )
    ),
    'orcid/author/' . $orcid_author->orcid_id,
    t('This action cannot be undone.'),
    t('Update'),
    t('Cancel')
  );
}

/**
 * Executes orcid author update.
 */
function orcid_author_update_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $orcid_author = orcid_author_load($form_state['values']['orcid_id']);
    $orcid_author = array_pop($orcid_author);
    $wrapper = entity_metadata_wrapper("OrcidAuthor", $orcid_author);
    // Save the model and go back to the add author screen.
    $transaction = db_transaction("orcid_author_import");
    try {
      $orcid_author->importAuthor();
      $wrapper->other_names->set($orcid_author->other_names);
      $wrapper->urls->set($orcid_author->urls);
      $orcid_author->save();

      cache_clear_all();
      watchdog('orcid', '@type: update %id.', array(
        '@type' => "ORCID Author",
        '%id' => $orcid_author->orcid_id,
      ));
      drupal_set_message(t('@type %given_name %surname (%id) has been updated.', array(
            '@type' => "ORCID Author",
            '%given_name' => $orcid_author->first_name,
            '%surname' => $orcid_author->last_name,
            '%id' => $orcid_author->orcid_id,
          )
        )
      );
    }
    catch (Exception $e) {
      $transaction->rollback();
      form_set_error("orcid_id", $e->getMessage());
    }

  }

  $form_state['redirect'] = 'orcid/author/' . $orcid_author->orcid_id;
}
