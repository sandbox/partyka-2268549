<?php

/**
 * @file
 * OrcidAuthor Entity definition.
 *
 * This entity holds the information as retrieved by the ORCID connector. It
 * holds all the information retrieved from ORCID. Currently this is holding
 * ONLY the publicly available information.
 */

class OrcidAuthor extends Entity {

  /**
   * Constructor Method.
   *
   * @param array $values
   *   Entity values.
   * @param string|null $entity_type
   *   Entity Type, ignored.
   */
  public function __construct(array $values = array(), $entity_type = NULL) {
    parent::__construct($values, 'OrcidAuthor');
  }

  /**
   * Get Entity Title.
   *
   * @return string
   *   Returns the author's name to use as the title.
   */
  public function getTitle() {
    return $this->first_name . " " . strtoupper($this->last_name);
  }

  /**
   * Get the author's ORCID URI.
   *
   * @return array
   *   Returns an ORCID URI in array format.
   */
  protected function defaultURI() {
    return array('path' => 'orcid/' . $this->identifier());
  }

  /**
   * Get this author's ORCID profile URL.
   *
   * @return string
   *   A fully-formed HTTP URL.
   */
  public function getOrcidURL() {
    $connector = OrcidConnector::getConnector();
    return $connector->getSelectedWWW() . "/" . $this->orcid_id;
  }

  /**
   * Import an author from the ORCID repository.
   *
   * @param OrcidConnector $connector
   *   Connector object.
   *
   * @return bool
   *   TRUE if import successful.
   */
  public function importAuthor(OrcidConnector $connector = NULL) {

    $success = FALSE;
    if ($connector == NULL) {
      $connector = OrcidConnector::getConnector();
    }

    $url = $connector->getProfileURI($this->orcid_id);

    $c = curl_init();

    curl_setopt($c, CURLOPT_URL, $url);
    curl_setopt($c, CURLOPT_HTTPHEADER, array("Accept: application/orcid+json"));
    curl_setopt($c, CURLOPT_RETURNTRANSFER, TRUE);

    $json = curl_exec($c);

    $http_code = curl_getinfo($c, CURLINFO_HTTP_CODE);
    if ($http_code != 200) {
      throw new Exception(t("Could not import researcher, received HTTP error code !code", array("!code" => $http_code)));
    }
    else {
      // Dumping the json object as an associative array.
      // Due to naming conflicts with valid PHP identifiers.
      $result = json_decode($json, TRUE);

      /*
       * The "@" error-suppression is deliberate as ORCID's response may not
       * have each and every element, depending on what the author has
       * decided to submit. Instead of many if-then statements, we're just
       * supressing an error that can result.
       */
      $this->first_name = @check_plain($result['orcid-profile']['orcid-bio']['personal-details']['given-names']['value']);
      $this->last_name = @check_plain($result['orcid-profile']['orcid-bio']['personal-details']['family-name']['value']);
      $this->credit_name = @check_plain($result['orcid-profile']['orcid-bio']['personal-details']['credit-name']['value']);
      $this->email = @check_plain($result['orcid-profile']['orcid-bio']['contact-details']['email'][0]['value']);
      $this->country = @check_plain($result['orcid-profile']['orcid-bio']['contact-details']['address']['country']['value']);
      $this->submission_date = @check_plain($result['orcid-profile']['orcid-history']['submission-date']['value']);
      $this->last_modified_date = @check_plain($result['orcid-profile']['orcid-history']['last-modified-date']['value']);
      $this->biography = @check_plain($result['orcid-profile']['orcid-bio']['biography']['value']);

      // Add in the other names.
      if (isset($result['orcid-profile']['orcid-bio']['personal-details']['other-names'])) {
        $this->other_names = $result['orcid-profile']['orcid-bio']['personal-details']['other-names']['other-name'];
      }
      else {
        $this->other_names = array();
      }

      if (isset($result['orcid-profile']['orcid-bio']['researcher-urls'])) {
        $this->urls = $result['orcid-profile']['orcid-bio']['researcher-urls']['researcher-url'];
      }
      else {
        $this->urls = array();
      }

      $success = TRUE;
    }

    curl_close($c);

    return $success;

  }

}
