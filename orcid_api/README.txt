CONTENTS OF THIS FILE
---------------------

* Summary
* Defaults

SUMMARY
-------
This is standalone and may be used by any other module to facilitate
integrations. The main ORCID module hasthis as a dependency, but
using ORCID module is not a requirement for using ORCID API.

Why would I use ORCID API but not the ORCID module?

You would use this if your institution does not need a full-fledged
Entity-level integration, or any other "full" ORCID integration,
but instead need to pull just something out of the API to put elsewhere
into your Drupal install.

DEFAULTS
--------
This module is currently wired to talk directly to pub.orcid.org.
There is a sandbox property in the OrcidConnector class, and if
you need to talk to the sandbox, switch the properties returned
in the getSelectedWWW() and getSelectedHost() methods.