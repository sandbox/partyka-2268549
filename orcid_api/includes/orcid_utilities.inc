<?php

/**
 * @file
 * This class is your typical utility Singleton class. Nothing special about it.
 */

class OrcidUtilities {

  /**
   * Calculate the check digit.
   *
   * Based on algorithm at
   * http://support.orcid.org/knowledgebase/articles/116780-structure-of-the-orcid-identifier
   *
   * @param string $base_digits
   *   Digits 1-15 (0-14).
   *
   * @return string
   *   the check digit, matching digit 16 (15)
   */
  public static function generateCheckDigit($base_digits) {
    $total = 0;
    $base_digits = strval($base_digits);
    for ($i = 0; $i < strlen($base_digits); $i++) {
      $digit = intval(substr($base_digits, $i, 1));
      $total = ($total + $digit) * 2;
    }
    $remainder = $total % 11;
    $result = (12 - $remainder) % 11;
    return $result == 10 ? "X" : intval($result);
  }

  /**
   * Check ORCID identifier.
   *
   * This method is the one to use to check to see if the ORCID id is
   * numerically correct. It should always be called first to see if an ID
   * is correct before making a call to the ORCID web service.
   *
   * @param string $id
   *   The ORCID id to check for validity.
   *
   * @return bool
   *   TRUE if the ID is numerically correct, FALSE otherwise.
   */
  public static function checkIdentifier($id) {
    $normalized_string = str_replace("-", "", $id);
    $check_digit = substr($normalized_string, 15);
    $normalized_string = substr($normalized_string, 0, 15);
    $calculated_digit = self::generateCheckDigit($normalized_string);
    if ($calculated_digit == $check_digit) {
      return TRUE;
    }
    return FALSE;
  }
}
