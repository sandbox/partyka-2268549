<?php

/**
 * @file
 * ORCID administrative functions, forms.
 */

/**
 * Allows an end-user to change the ORCID endpoints.
 */
function orcid_api_configuration($form, &$form_state) {

  $form['orcid_api_explain'] = array(
    '#markup' => t('The default values are sufficient if you are
    integrating directly with ORCID. They only need to be changed if you wish
    to test your integration against the sandbox or a development instance.'),
  );

  $form['orcid_api_url'] = array(
    '#title' => t('ORCID URL'),
    '#type' => 'textfield',
    '#description' => t('Please enter the main orcid domain, such as http://orcid.org/ or http://sandbox.orcid.org/. If In doubt, leave as is.'),
    '#required' => TRUE,
    '#default_value' => variable_get('orcid_api_url', 'orcid.org/'),
  );
  $form['orcid_api_public_url'] = array(
    '#title' => t('ORCID Public API URL'),
    '#type' => 'textfield',
    '#description' => t('Please enter the public orcid api domain. If in doubt, leave as is.'),
    '#required' => TRUE,
    '#default_value' => variable_get('orcid_api_public_url', 'pub.orcid.org/'),
  );

  return system_settings_form($form);
}

/**
 * Validate the URLs as responding to a request for an orcid bio.
 */
function orcid_api_configuration_validate($form, &$form_state) {
  // Test public api uri.
  $values = $form_state['values'];
  $url = "http://" . $values['orcid_api_public_url'];
  $url .= '/search/orcid-bio';
  $response = drupal_http_request(url($url));
  if ($response->code !== "200") {
    form_set_error('orcid_api_public_url', t('Public API URL is not correct. No results found on "test" search.'));
  }
}
