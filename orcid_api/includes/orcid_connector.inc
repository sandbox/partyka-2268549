<?php

/**
 * @file
 * Provides a connection to the ORCID repository.
 *
 * This file holds a singleton responsible for determining the connection
 * info to the ORCID repository.
 * It is done specifically as a singleton vs a static class to allow a
 * site developer implementing ORCID integration the capability to add
 * site-specific information if necessary. This would be useful if there
 * is an internal HR database, for example.
 */

class OrcidConnector {

  private $sandboxHost = "pub.sandbox.orcid.org";
  private $publicHost = "pub.orcid.org";
  private $sandboxWWW = "sandbox.orcid.org";
  private $publicWWW = "orcid.org";
  private static $connector = NULL;
  private $apiVersion = "v1.1";
  private $protocol = "http://";
  private $memberToken = NULL;

  /**
   * Get the URL for the sandbox repository web service.
   *
   * @return string
   *   Sandbox Webservice URL.
   */
  public function getSandboxHost() {
    return $this->sandboxHost;
  }

  /**
   * Get the URL for the public repository web service.
   *
   * @return string
   *   Public Webservice URL.
   */
  public function getPublicHost() {
    return $this->publicHost;
  }

  /**
   * Get Public Website Base URL for ORCID.
   *
   * @return string
   *   The ORCID Public Website URL.
   */
  public function getPublicWWW() {
    return $this->$publicWWW;
  }

  /**
   * The Sandbox URL.
   *
   * @return string
   *   The web address for the Sandbox repository.
   */
  public function getSandboxWww() {
    return $this->sandboxWWW;
  }

  /**
   * Get the API version this connector is aware of.
   *
   * @return string
   *   The API version.
   */
  public function getApiVersion() {
    return $this->apiVersion;
  }

  /**
   * Get the currently selected web protocol.
   *
   * @return string
   *   HTTP or HTTPS.
   */
  public function getProtocol() {
    return $this->protocol;
  }

  /**
   * Constructor.
   */
  private function __construct() {
  }

  /**
   * Returns a connector object to the currently set ORCID repository.
   */
  public static function getConnector() {
    if (self::$connector == FALSE) {
      self::$connector = new OrcidConnector();
    }

    return self::$connector;
  }

  /**
   * Get the Current ORCID host to link to.
   *
   * @return string
   *   The Current ORCID host to link to
   */
  public function getSelectedHost() {
    return variable_get("orcid_api_public_url", "pub.orcid.org");
  }

  /**
   * Get the Current ORCID webserver to link to.
   *
   * @return string
   *   The Current ORCID webserver to link to
   */
  public function getSelectedWWW() {
    return variable_get("orcid_api_url", "orcid.org");
  }

  /**
   * Get a Bio URI.
   *
   * @param string $id
   *   ORCID Id.
   *
   * @return string
   *   The ORCID Web Service URI to retrieve an ORCID Bio.
   */
  public function getBioURI($id) {
    return $this->protocol . "{$this->getSelectedHost()}/{$this->getApiVersion()}/{$id}/orcid-bio";
  }

  /**
   * Get a Profile URI.
   *
   * @param string $id
   *   ORCID Id.
   *
   * @return string
   *   The ORCID Web Service URI to retrieve an ORCID profile.
   */
  public function getProfileURI($id) {
    return $this->protocol . "{$this->getSelectedHost()}/{$this->getApiVersion()}/{$id}/orcid-profile";
  }
}
