<?php
/**
 * @file
 * Example template for an Orcid Author template.
 */
?>

<div id="orcid-author-<?php print $OrcidAuthor->orcid_id; ?>"
     class="orcid-author">
  <span class="orcid-id">
    <?php print $OrcidAuthor->orcid_id; ?>
  </span><br/>
  <span class="orcid-given-name">
    <?php print $OrcidAuthor->first_name; ?>
  </span>
  <span class="orcid-family-name">
    <?php print $OrcidAuthor->last_name; ?>
  </span><br/>
  <span class="orcid-email">
    <a href="mailto:<?php print $OrcidAuthor->email; ?>">
      <?php print $OrcidAuthor->email; ?>
    </a>
  </span><br/>
  <span class="orcid-credit">
    I also go by: <?php print $OrcidAuthor->credit_name ?>
  </span><br/>
  <span class="orcid-country">
    Country: <?php print $OrcidAuthor->country; ?>
  </span><br/>
  <span class="orcid-researcher-url">
    <a href="http://<?php print $OrcidAuthor->getOrcidURL(); ?>">
      Researcher Profile on ORCID
    </a>
  </span>
</div>

<div id="orcid-biography">
  <?php print $OrcidAuthor->biography; ?>
</div>
