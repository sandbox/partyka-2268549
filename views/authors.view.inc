<?php

/**
 * @file
 * This file provides a default listing of ORCID authors.
 */

$view = new view();
$view->name = 'orcid_members';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'orcid_author';
$view->human_name = 'Orcid Members';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Orcid Members';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'entity_id' => 'entity_id',
  'country' => 'country',
  'email' => 'email',
  'first_name' => 'first_name',
  'last_name' => 'last_name',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'entity_id' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'country' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'email' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'first_name' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'last_name' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Field: ORCID Author: Orcid_id */
$handler->display->display_options['fields']['orcid_id']['id'] = 'orcid_id';
$handler->display->display_options['fields']['orcid_id']['table'] = 'orcid_author';
$handler->display->display_options['fields']['orcid_id']['field'] = 'orcid_id';
$handler->display->display_options['fields']['orcid_id']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['orcid_id']['alter']['text'] = '<a href="/orcid/author/[orcid_id]">[orcid_id]</a>  (<a href="http://orcid.org/[orcid_id]">ORCID</a>)';
/* Field: ORCID Author: Country */
$handler->display->display_options['fields']['country']['id'] = 'country';
$handler->display->display_options['fields']['country']['table'] = 'orcid_author';
$handler->display->display_options['fields']['country']['field'] = 'country';
/* Field: ORCID Author: Email */
$handler->display->display_options['fields']['email']['id'] = 'email';
$handler->display->display_options['fields']['email']['table'] = 'orcid_author';
$handler->display->display_options['fields']['email']['field'] = 'email';
/* Field: ORCID Author: First_name */
$handler->display->display_options['fields']['first_name']['id'] = 'first_name';
$handler->display->display_options['fields']['first_name']['table'] = 'orcid_author';
$handler->display->display_options['fields']['first_name']['field'] = 'first_name';
/* Field: ORCID Author: Last_name */
$handler->display->display_options['fields']['last_name']['id'] = 'last_name';
$handler->display->display_options['fields']['last_name']['table'] = 'orcid_author';
$handler->display->display_options['fields']['last_name']['field'] = 'last_name';

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'orcid-members';
