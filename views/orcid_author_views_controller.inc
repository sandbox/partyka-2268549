<?php

/**
 * @file
 * This class defines the view handlers for date field types.
 *
 * Also defines relationship handlers for the normalized data tables in the
 * module.
 */

class OrcidAuthorViewsController extends EntityDefaultViewsController {

  /**
   * Overrides default views functionality.
   *
   * @return array
   *   Updated views data.
   */
  public function views_data() {

    $data = parent::views_data();

    // The submission date and last modified date are unix timestamps.
    $data['orcid_author']['submission_date']['field']['handler'] = 'views_handler_field_date';
    $data['orcid_author']['last_modified_date']['field']['handler'] = 'views_handler_field_date';

    // Need to set views handlers here for the lists of Other Names.
    $data['orcid_author']['other_names'] = array(
      'title' => t('Other Names'),
      'help' => t('Other Names use by this Orcid Author.'),
      'relationship' => array(
        'base' => 'orcid_author_other_names',
        'base field' => 'orcid_id',
        'relationship field' => 'orcid_id',
        'handler' => 'views_handler_relationship',
        'click sortable' => 'false',
      ),
    );

    $data['orcid_author']['urls'] = array(
      'title' => t('URLs'),
      'help' => t('URLs used by this Orcid Author.'),
      'relationship' => array(
        'base' => 'orcid_author_urls',
        'base field' => 'orcid_id',
        'relationship field' => 'orcid_id',
        'handler' => 'views_handler_relationship',
        'click sortable' => 'false',
      ),
    );

    $data['orcid_author_other_names']['table'] = array(
      'group' => 'ORCID Author',
      'join' => array(
        'orcid_author' => array(
          'left_field' => 'orcid_id',
          'field' => 'orcid_id',
        ),
      ),
    );

    $data['orcid_author_other_names']['other_name'] = array(
      'title' => t('Other Names'),
      'help' => t('Other Names as Used by the author'),
      'field' => array(
        'real field' => 'other_name',
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
    );

    // Set views handlers here for the lists of URLs.
    $data['orcid_author_urls']['urls'] = array(
      'title' => t('Author URLs'),
      'help' => t('URLs the author has listed on the ORCID site'),
      'relationship' => array(
        'base' => 'orcid_author_other_names',
        'base field' => 'orcid_id',
        'relationship field' => 'orcid_id',
        'handler' => 'views_handler_relationship',
        'click sortable' => 'false',
      ),
    );

    $data['orcid_author_urls']['table'] = array(
      'group' => 'ORCID Author',
      'join' => array(
        'orcid_author_urls' => array(
          'left_field' => 'orcid_id',
          'field' => 'orcid_id',
        ),
      ),
    );

    $data['orcid_author_urls']['url'] = array(
      'title' => t('Author URL'),
      'help' => t('URLs the author has listed on the ORCID site'),
      'field' => array(
        'real field' => 'url',
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
    );

    $data['orcid_author_urls']['title'] = array(
      'title' => t('Author URL titles'),
      'help' => t('The title to correspond with a URL the author has entered.'),
      'field' => array(
        'real field' => 'url',
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
    );

    return $data;

  }

}
