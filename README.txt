
ORCID Entity Module
-----------------
by Jason Partyka  (https://drupal.org/user/344048)

The ORCID Entity Module retrieves information from the ORCID repository.

The main and primary purpose of this module, at this time, is to cache
information from the ORCID repository for display in a Drupal site. Why should
you use this module?

1) It provides an entity-based representation that is, as appropriate as
   possible, a representation of the way it is retrieved from ORCID.

2) As it is based on the Entity API, you are able to take advantage of any
   integration automatically provided by Entity API, such as views. This
   will be powerful for building up robust and dynamic pages documenting
   your researchers' exploits, er, achievements :-).

3) You can use modules like Entity Reference to easily link your own Profile
   or User pages (which have probably been heavily customized!) to the ORCID
   entities without unnecessarily cluttering up your already existing entities.

4) More likely than not - you don't want to show everything from ORCID, just
   bits and pieces. Using views and tpl.php files you can control what gets
   displayed. The database itself will still cache as much information as
   it can so you can easily add new data in the future if and when desired.

5) "big picture" -- Entities aren't going away, although they are substantially
    changed in D8, so this will help keep your site prepared for the future.


--------------------------------------------------------------------------------
                                Architecture
--------------------------------------------------------------------------------
This module is designed with the goal of displaying ORCID's data as an integral
part of your Drupal-based website. It's not designed or intended to replicate
ORCID's data into your Drupal instance. It is designed to allow easy and
quick integration into Drupal's data processing capabilities.

Therefore:
    * There is no guarantee that your cached data is identical to ORCID's
      repository. If an update to a particular entity is needed, it may
      be triggered by:
        1) Log in as a user with the 'update orcid [type]' permission.
        2) Click on the "Update" tab, and confirm you wish to update.
    * No promise is being made that the data you wish to pull out of ORCID
      is supported by this module. That said, if you find the data is missing,
      please file an issue in the issue queue. Of course, please feel free to
      do it yourself and submit a patch! Patches are cheerfully and gratefully
      accepted.
    * TODO:
        1) Create a cron job/cache timeout to automatically update the entities
           regularly.

--------------------------------------------------------------------------------
                                Public API
--------------------------------------------------------------------------------

  * ORCID has a public API that requires no authorization or authentication to
    access.

  * Thus the module provides API functions like entity_save(), entity_create(),
    entity_delete(), entity_revision_delete(), entity_view() and entity_access()
    among others.
    entity_load(), entity_label() and entity_uri() are already provided by
    Drupal core.

  *  For more information about how to provide this metadata, have a look at the
     API documentation, i.e. entity_metadata_hook_entity_info().



--------------------------------------------------------------------------------
                            Private API
--------------------------------------------------------------------------------

 * There is no current Private/Member only support in this module.


--------------------------------------------------------------------------------
                           Getting Started
--------------------------------------------------------------------------------
NOTE: This section is subject to heavy change as the module nears a release
state.

Currently, the ORCID production server is the default server the module
integrates against. You will not need to change this unless you wish to
test against the sandbox or are running your own developmental ORCID server.

At present, the only way to bring an author into the system is to go to
orcid/add/author. If the ORCID ID entered here is valid it will try to retrieve
data from ORCID and store it in the local tables. Presently, there is no
check to see if this ID has been claimed or not. @BTMash's functionality
to see if the ID is claimed will soon be ported into this module.

To check to see if your installation has been successful, follow these steps:

1) Go to orcid.org and type a name into its search field. If you have a surname
that isn't very common, like "Smith", it's fun to try your own name :-).

2) Copy the ORCID ID into the field at orcid/add/author. Save it, and you'll be
taken to a page where you can see what was imported. This page is the
orcid_author.tpl.php template. It's meant to be an example template and it is
highly encouraged that you customize this to fit your theme.

