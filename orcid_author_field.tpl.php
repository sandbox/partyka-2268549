<?php
/**
 * @file
 * Template for author field entity reference list.
 */
?>
<div class='<?php print $classes; ?>'>
  <a href="<?php print $orcid_url; ?>">
    <?php print $orcid_id->first_name; ?> <?php print $orcid_id->last_name; ?>
  </a>
</div>
