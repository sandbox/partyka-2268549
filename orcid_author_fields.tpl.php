<?php
/**
 * @file
 * Template for single author field entity reference value.
 */
?>
<div class='<?php print $classes; ?>'>
  <ul>
    <?php foreach ($orcid_ids as $author) : ?>
      <li><?php print render($author); ?></li>
    <?php endforeach; ?>
  </ul>
</div>
